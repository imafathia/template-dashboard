# bootstrap template

## cara install

Our goal is to make it installable on different Package Manager! Do you want to use it on your favorite Package Manager and you know how? Pull request all the way! 

As of now, this is some installation available:

**Bower**

```
bower install gentelella --save
```

**npm**

```
npm install gentelella --save
```
## How to contribute
To contribute, please ensure that you have stable [Node.js](https://nodejs.org/) and [npm](https://npmjs.com) installed.

Test if Gulp CLI is installed by running `gulp --version`.  If the command isn't found, run `npm install -g gulp`.  For more information about installing Gulp, see the Gulp's [Getting Started](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md).

To have all gulp dependencies run ```npm install```

If `gulp` is installed, follow the steps below.

1. Fork and clone the repo.
2. Run `gulp`, this will open gentelella on your default browser
3. Now you can code, code and code!
4. Submit a pull request

## Scripts included:
* Bootstrap
* Font Awesome
* jQuery-Autocomplete
* FullCalendar
* Charts.js
* Bootstrap Colorpicker
* Cropper
* dataTables
* Date Range Picker for Bootstrap
* Dropzone
* easyPieChart
* ECharts
* bootstrap-wysiwyg
* Flot - Javascript plotting library for jQuery.
* gauge.js
* iCheck
* jquery.inputmask plugin
* Ion.RangeSlider
* jQuery
* jVectorMap
* moment.js
* Morris.js - pretty time-series line graphs
* PNotify - Awesome JavaScript notifications
* NProgress
* Pace
* Parsley
* bootstrap-progressbar
* select2
* Sidebar Transitions - simple off-canvas navigations
* Skycons - canvas based wather icons
* jQuery Sparklines plugin
* switchery - Turns HTML checkbox inputs into beautiful iOS style switches
* jQuery Tags Input Plugin
* Autosize - resizes text area to fit text
* validator - HTML from validator using jQuery
* jQuery Smart Wizard